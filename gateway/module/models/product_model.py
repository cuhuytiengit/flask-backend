from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:cuhuytien@192.168.0.144/flask_backend'

db = SQLAlchemy(app)
migrate =Migrate(app, db)
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    productname = db.Column(db.String(80), unique=True, nullable=False)
    price = db.Column(db.Integer, unique=True, nullable=False)
    image = db.Column(db.String(180), unique=True, nullable=False)
    description = db.Column(db.String(250), unique=True, nullable=False)

    def __init__(self,   productname, price, image,description):
        self.  productname =   productname
        self.price = price
        self.image = image
        self.description = description